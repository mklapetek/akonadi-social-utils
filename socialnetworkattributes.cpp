/*
  Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "socialnetworkattributes.h"

#include <QString>

#include <qjson/serializer.h>
#include <qjson/parser.h>

Akonadi::SocialNetworkAttributes::SocialNetworkAttributes( const QString &userName, const QString &networkName, bool canPublish, uint maxPostLength )
{
  mAttributes["userName"] = userName;
  mAttributes["networkName"] = networkName;
  mAttributes["canPublish"] = canPublish;
  mAttributes["maxPostLength"] = maxPostLength;
}

Akonadi::SocialNetworkAttributes::~SocialNetworkAttributes()
{
}

void Akonadi::SocialNetworkAttributes::deserialize( const QByteArray &data )
{
  QJson::Parser parser;
  mAttributes = parser.parse(data).toMap();
}

QByteArray Akonadi::SocialNetworkAttributes::serialized() const
{
  QJson::Serializer serializer;
  return serializer.serialize(mAttributes);
}

Akonadi::Attribute* Akonadi::SocialNetworkAttributes::clone() const
{
  return new SocialNetworkAttributes( mAttributes["userName"].toString(),
                                      mAttributes["networkName"].toString(),
                                      mAttributes["canPublish"].toBool(),
                                      mAttributes["maxPostLength"].toUInt() );
}

QByteArray Akonadi::SocialNetworkAttributes::type() const
{
  return "socialattributes";
}

QString Akonadi::SocialNetworkAttributes::userName() const
{
  return mAttributes["userName"].toString();
}

QString Akonadi::SocialNetworkAttributes::networkName() const
{
  return mAttributes["networkName"].toString();
}

bool Akonadi::SocialNetworkAttributes::canPublish() const
{
  return mAttributes["canPublish"].toBool();
}

uint Akonadi::SocialNetworkAttributes::maxPostLength() const
{
  return mAttributes["maxPostLength"].toUInt();
}
